# WORDLE

## Index

- ¿Que es Wordle?
- ¿Como descargar Wordle?
- Funcionamiento del programa


## ¿Que es Wordle?

Wordle es un juego de adivinar palabras, que tiene un formato de crucigrama y con similitudes con otros juegos como por
ejemplo Mastermind. 

En el, tienes que adivinar una palabra en seis intentos, en los que no se te dan más pistas que decirte qué letras de 
las que has puesto están dentro de la palabra.

![Imagen de muestra de Wordle](Img/Wordle-que-es.jpg)



## COMO DESCARGAR E INICIAR WORDLE


Lo primero que tendrá que hacer es seleccionar el icono de la izquierda del botón azul llamado "Clone".

Se le abrirá un desplegable en el que le dará diferentes opcciones. El proyecto se descargará comprimido.



![img_1.png](/Img/Paso%201.PNG)

Una vez descargado, lo que tendrá que hacer es descomprimir su archivo y le aparecerá una carpeta llamada
**wordle_lorenzopoderoso**.

Una vez finalizada extraída la carpeta **wordle_lorenzopoderoso**, dale click derecho a ella y seleccione la opción
**Open Folder as IntelliJ IDEA Project**.

![img_2.png](Img/abrir%20con%20intellij.PNG)

Se le abrirá el proyecto con sus respectivas carpetas. Para poder probarlo deberá dirigirse a:

**src/Wordle.kt**

![img_3.png](Img/direcciónWordle.PNG)

Verá todo el código del programa, para evitar ver las posibles palabras que hay en el diccionario le recomiendo deslizar
el códico hasta abajo del todo.

Una vez seguido todo estos pasos, arriba a la izquiera de saldrá un icono en verde para iniciar el programa:

![img_4.png](Img/play.PNG)

Una vez lo ejecute, le saldrá una terminal en la que le saldrá este mensaje:

![img_5.png](Img/Inicio.PNG)

##### Una vez le salga este mensaje, pulse 1 y.. **¡A JUGAR!**

## Funcionamiento del programa

#### -- Variables utilizadas --
![Variables](Img/variables.PNG)

- **val dictionary = arrayOf**: Es la variable encargada de almacenar **todas** las palabras.
- **val repeatedWords**: Es la lista mutable encargada de almacenar las palabras introducidas con el fin de que no se repitan.
- **var intents**: Es la variable encargada de controlar los intentos disponibles. El juego dispone de **6** intentos. Cuando
esta variable llegue a 0 y no se haya descubierto la palabra seleccionada, el programa se acabará.

Las otras variables son las responsables de añadir el color.

#### -- Comienzo del juego --
Para empezar, he decidido poner un bucle do-while con el objetivo de poder iniciar el juego cuando pulses "1" :

![Imagen de muestra de Wordle](Img/Inicio.PNG)

Una vez pulse 1, el programa lanzará una pregunta: 

![Imagen de muestra de Wordle](Img/Comienzo.PNG)

La palabra introducida se almacenará en la variable "palabra".

### Lógica del juego

El programa está dividido en dos bloques dentro de un do-while.

#### Bloque 1
El primer bloque comprueba si la palabra introducida tiene un tamaño de 5 letras y si está metida dentro de la lista 
mutable.

En el caso que se cumplan esas dos condiciones, el programa iniciará un bucle "for" en el que irá comprobando letra por
letra si la palabra escogida aleatoriamente coincide con la palabra introducica.

**Traducción:**
- Si coincide, el programa printará la letra en verde.
- Si la letra está en las dos palabras pero no en su posición, el programa printará la letra en amarillo.
- Si la letra **NO** está en la palabra que se tiene que acertar, el programa printará la letra en gris.

Cada vez que el programa haga el proceso se le irá restando 1 a la variable "intents" gracias a (--intents).

Los prints se encargarán de añadirle algo de "estética" a la terminal y a su vez informar de los intentos que le quedan
al usuario para acertar la palabra.

![Imagen de muestra de Wordle](Img/Comprobación%20de%20tamaño%20y%20palabra%20repetida.PNG)


#### Bloque 2

El segundo bloque se encarga de mostrar en la terminal si la palabra no tiene un tamaño de 5 letras, si la palabra está 
repetida o la palabra introducida es la acertada.

**Traducción**
- Si la palabra no tiene 5 letras, el programa printrá: " Introduzca una palabra de 5 letras "
- Si la palabra está repetida, el programa printará: " ¡Esta palabra ya la has intentado! "
- Si la palabra introducida es la correcta, el programa printará: " ***** ¡FELICIDADES HAS GANADO! ***** "

![Imagen de muestra de Wordle](Img/Prints.PNG)



