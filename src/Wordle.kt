import java.util.*
import kotlin.system.exitProcess

//VARIABLES
val scanner = Scanner(System.`in`)
val wordList = listOf("casar", "besar", "coger", "jugar", "temas", "silla", "movil", "tecla", "suiza", "nariz", "bolsa", "cable", "gesto", "movil", "trapo", "tabla", "noche", "carta", "lapiz", "sueño")
var randomWord = wordList.random()
val usedWords: MutableList<String> = mutableListOf("")
var intents = 6
val colorVerde = "\u001B[32m"
val backgroundGreenColor = "\u001B[42m"
val colorAmarillo = "\u001B[33m"
val backgroundYellowColor = "\u001B[43m"
val colorRed = "\u001b[31m"
val backgroundGrayColor = "\u001B[47m"
val colorReset = "\u001B[0m"


//MENÚ
fun wordleMenu(){
    println("******************")
    println("****BIENVENIDO****")
    println("********A*********")
    println("******WORDLE******")
    println("******************")

    do {
        print("$backgroundGrayColor 0. Salir $colorReset")
        println("$backgroundGreenColor 1. Jugar $colorReset")
        val playerDecision = scanner.nextInt()

        if (playerDecision == 1){ //Quiere jugar
            wordleTutorial()
        }
        else if (playerDecision == 0) { //No quiere jugar
            exitGame()
        }else{
            println("$colorRed El dato introducido no es correcto. Por favor, introduzca una opción válida. $colorReset")
        }

    }while (playerDecision != 1)

}
fun wordleTutorial(){
    println("Wordle es un juego de adivinar palabras, que tiene un formato de \n" +
            "crucigrama y con similitudes con otros juegos como el Mastermind.\n" +
            " En él, tienes que adivinar una palabra en seis intentos, en los \n" +
            "que no se te dan más pistas que decirte qué letras de las que has\n" +
            " puesto están dentro de la palabra.")
    println()

    print("$backgroundGrayColor 0. Salir $colorReset")
    println("$backgroundGreenColor 1. Comenzar $colorReset")
    val playerDecision = scanner.nextInt()

    if (playerDecision == 1){ //Quiere continuar
        println("$backgroundGreenColor Empezando el juego... $colorReset")
        println()
        println()
    }else if (playerDecision == 0) { //No quiere jugar
        exitGame()
    }else{
        println("$colorRed El dato introducido no es correcto. Por favor, introduzca una opción válida.")
    }

}
fun exitGame() {
    println("$backgroundGrayColor Saliendo del juego... $colorReset")
    exitProcess(0)
}

//COMPROBACIÓN DE PALABRA
fun checkWord(): String{
    val palabra = readLine()!!

    if (palabra.length != 5) {
        println("$colorRed Palabra no valida, por favor ingrese una palabra de 5 letras. $colorReset")
        return checkWord()
    } else if (usedWords.contains(palabra)) {
        println("$colorRed Palabra ya ingresada, ingrese una nueva $colorReset")
        return checkWord()
    } else {
        usedWords.add(palabra)
        return palabra
    }
}

//COMPARACIÓN CON PALABRA RANDOM
fun compareWithRandomWord(palabra: String, dictionary: String) {
    val comparedLetters = mutableSetOf<Char>()

    if (palabra.length == 5 && palabra !in randomWord) {
        for (i in palabra.indices){
            if (palabra[i] in comparedLetters) {
                print("$backgroundGrayColor ${palabra[i]} $colorReset")
                continue
            }
            comparedLetters.add(palabra[i])
            if (dictionary.indexOf(palabra[i]) != -1) {
                if(dictionary[i] == palabra[i]){
                    print("$backgroundGreenColor ${palabra[i]} $colorReset")
                }else{
                    print("$backgroundYellowColor ${palabra[i]} $colorReset")
                }
            }
            else {
                print("$backgroundGrayColor ${palabra[i]} $colorReset")
            }
        }
        --intents
        print(" | Te quedan $colorRed $intents $colorReset intentos.")
        println("")
    }
}

//FIN DEL JUEGO
fun endGame(){
    if (intents == 0){
        println("¡HAS PERDIDO! Palabra correcta:  $colorRed ${randomWord.uppercase()} $colorReset")
    } else{
        println("¡HAS GANADO CON $backgroundGreenColor $intents $colorReset fallos!")
    }
}

//Reinicio Wordle
fun resetGame(){
    intents = 6
    usedWords.clear()
    randomWord = wordList.random()
    usedWords.clear()
}


fun main(){
    var nuevoJuego = true

    while (nuevoJuego) {
        wordleMenu()
        while (intents > 0) {
            val palabra = checkWord()
            compareWithRandomWord(palabra,randomWord)
            if (palabra == randomWord){
                println("$backgroundGreenColor $randomWord $colorReset")
                break
            }
        }
        endGame()

        println("¿Quieres volver a jugar? (s/n)")
        val playerDecision = readLine()!!

        if (playerDecision == "s"){
            nuevoJuego = playerDecision == "s"
            resetGame()
            wordleMenu()
        }
        else if (playerDecision == "n"){
            println("$backgroundGrayColor Saliendo del juego... $colorReset")
        }else {
            println("$colorRed Palabra no valida, por favor ingrese una palabra de 5 letras. $colorReset")
        }
    }
}